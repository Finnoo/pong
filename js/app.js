var
        canvasInfo, ctxInfo,
        canvasMain, ctxMain,
        WIDTH_MAIN, HEIGHT_MAIN,
        WIDTH_INFO, HEIGHT_INFO,
        keyData = null, mouseDirectionData = null, mouseWheelData = null, playWith, mobile = false,
        mousePosY = null,
        lost = null, gameBreak = null, playSounds = null, refreshBefore = false,
        ki = null, diffecultDegree = null,
        startTime = null, lastTime = null,
        poinstsToWinn = null, shootCount = null,
        OPTIONS = {BALL_DIRECTION_UP: "up",
    BALL_DIRECTION_DOWN: "down",
    BALL_DIRECTION_RIGHT: "right",
    BALL_DIRECTION_LEFT: "left",
    PLAY_WITH_KEYBOARD: "keyboard",
    PLAY_WITH_MOUSE_WHEEL: "wheel",
    KEY_UP: 38,
    KEY_DOWN: 40,
    KEY_SPACE: 32,
    MOUSE_DOWN: 1,
    MOUSE_UP: 0,
    MOUSE_WHEEL_UP: 0,
    MOUSE_WHEEL_DOWN: 1},
IMAGES = {
    PLAYER_ONE: {PATH: "playerOne", WIDTH: 4, HEIGHT: 100},
    PLAYER_TWO: {PATH: "playerTwo", WIDTH: 4, HEIGHT: 100},
    BACKGROUND: {PATH: "background", WIDTH: WIDTH_MAIN, HEIGHT: HEIGHT_MAIN},
    BALL: {PATH: "ball", WIDTH: 30},
    BULLET: {PATH: "bullet", WIDTH: 10},
    BOX_SMALL_GREEN: {PATH: "box_small_green"},
    BOX_LARGE_GREEN: {PATH: "box_large_green"},
    BOX_SMALL_BLUE: {PATH: "box_small_blue"},
    BOX_LARGE_BLUE: {PATH: "box_large_blue"},
    BOX_SMALL_RED: {PATH: "box_small_red"},
    BOX_LARGE_RED: {PATH: "box_large_red"},
    MOUSE: {PATH: "mouse"},
    KEYBOARD: {PATH: "keyboard"}},
BOX_SIZES = {
    BOX_SMALL: {WIDTH: 60, HEIGHT: 60},
    BOX_LARGE: {WIDTH: 60, HEIGHT: 120}
},
SOUNDS = {BACKGROUND: new Audio("sounds/background.wav"),
    PADDEL: new Audio("sounds/paddel.wav"),
    WAND: new Audio("sounds/wand.wav"),
    LASER_GUN: new Audio("sounds/laser_gun.mp3"),
    EXPLOSION: new Audio("sounds/explosion.wav"),
    GAME_OVER: new Audio("sounds/game_over.wav"),
    RELOAD_ONE_SEC: new Audio("sounds/reload_1.mp3"),
    RELOAD_TWO_SEC: new Audio("sounds/reload_2.mp3"),
    RELOAD_THREE_SEC: new Audio("sounds/reload_3.mp3")},
bullets = [], boxes = [];

function main(nameOne, nameTwo, playWithMouseWheel) {
    playerOne.name = nameOne;
    playerOne.name = nameTwo;
    playWith = OPTIONS.PLAY_WITH_MOUSE_WHEEL;
    canvasInfo = document.getElementById("canvasInfo");
    ctxInfo = canvasInfo.getContext("2d");

    canvasMain = document.getElementById("canvasMain");
    ctxMain = canvasMain.getContext("2d");
    WIDTH_MAIN = canvasMain.width;
    HEIGHT_MAIN = canvasMain.height;
    WIDTH_INFO = canvasInfo.width;
    HEIGHT_INFO = canvasInfo.height;
    init();
    mouseMove();
    mouseWheelMove();
    var loop = function() {
        if (!lost) {
            if (!gameBreak) {
                refreshBefore = false;
                updateGame();
                if (playSounds) {
                    SOUNDS.BACKGROUND.play();
                    updateInfo("Pause: esc          Audio aus: s");
                }
                else {
                    updateInfo("Pause: esc          Audio ein: s");
                }
            }
            else {
                startTime = new Date().getTime();
                lastTime = -3;
                if (!refreshBefore) {
                    updateInfo("Weiterspielen: esc");
                    SOUNDS.BACKGROUND.pause();
                    refreshBefore = true;
                }
            }
        }
        window.requestAnimationFrame(loop);
    };
    window.requestAnimationFrame(loop);
}

function init() {
    playerOne.x = 20;
    playerOne.y = HEIGHT_MAIN / 2 - playerOne.height;
    playerOne.speed = 6.5;
    playerOne.points = 0;
    playerOne.coins = 0;

    playerTwo.x = WIDTH_MAIN - 20;
    playerTwo.y = HEIGHT_MAIN / 2 - playerTwo.height;
    playerTwo.speed = 1.5;
    playerTwo.points = 0;
    playerTwo.coins = 0;

    ball.x = WIDTH_MAIN / 2;
    ball.y = HEIGHT_MAIN / 2;
    ball.speed = 1.55;
    ball.directionX = OPTIONS.BALL_DIRECTION_RIGHT;
    ball.directionY = OPTIONS.BALL_DIRECTION_UP;
    ball.angle = 1.5;
    ball.angleMax = 2;
    ball.angleMin = 0.5;

    time.x = WIDTH_INFO - 150;
    time.y = HEIGHT_INFO / 2;
    time.sec = 0;

    message.x = 10;
    message.y = HEIGHT_INFO / 2;
    message.message = "";

    coins.x = WIDTH_INFO / 2 - 50;
    coins.y = HEIGHT_INFO / 2;

    gameOver.x = 100;
    gameOver.y = HEIGHT_MAIN / 2;

    playWithInfo.x = WIDTH_INFO / 2 - 145;
    playWithInfo.y = HEIGHT_INFO / 2 - 20;

    diffecultDegree = 20;
    lost = false;
    gameBreak = false;
    playSounds = true;
    ki = true;
    startTime = new Date().getTime();
    pointsToWinn = 3;
    lastTime = -3;
    shootCount = 3;
}

playerOne = {
    x: null,
    y: null,
    width: IMAGES.PLAYER_ONE.WIDTH,
    height: IMAGES.PLAYER_ONE.HEIGHT,
    speed: null,
    name: null,
    points: null,
    coins: null,
    update: function() {

        if (mobile) {
            if (mousePosY < this.y + (this.height / 2) - 5 || mousePosY > this.y + this.height + 5) {
                if (mouseDirectionData === OPTIONS.MOUSE_UP && this.y - this.speed > 0 - 10) {
                    this.y -= this.speed;
                }
                if (mouseDirectionData === OPTIONS.MOUSE_DOWN && this.y + this.speed < HEIGHT_MAIN - this.height + 10) {
                    this.y += this.speed;
                }
            }
        } else if (playWith === OPTIONS.PLAY_WITH_MOUSE_WHEEL) {
            if (mouseWheelData === OPTIONS.MOUSE_WHEEL_UP && this.y - this.speed > 0 - 10) {
                this.y -= this.speed;
            }
            if (mouseWheelData === OPTIONS.MOUSE_WHEEL_DOWN && this.y + this.speed < HEIGHT_MAIN - this.height + 10) {
                this.y += this.speed;
            }
        }
        else if (playWith === OPTIONS.PLAY_WITH_KEYBOARD) {
            if (keyData === OPTIONS.KEY_UP && this.y - this.speed > 0 - 10) {
                this.y -= this.speed;
            }
            if (keyData === OPTIONS.KEY_DOWN && this.y + this.speed < HEIGHT_MAIN - this.height + 10) {
                this.y += this.speed;
            }
        }
    },
    draw: function() {
        ctxMain.drawImage(document.getElementById(IMAGES.PLAYER_ONE.PATH), this.x, this.y);
    },
    touch: function() {
        var playerOnePixels = [];
        var ballPixels = [];
        for (var i = 0; i < this.height; i++) {
            playerOnePixels.push(new Point(this.x + this.width, this.y + i));
        }
        for (i = 0; i < ball.width / 2; i++) {
            for (var j = 0; j < ball.width; j++) {
                ballPixels.push(new Point(ball.x + i, ball.y + j));
            }
        }
        for (i = 0; i < playerOnePixels.length; i++) {
            for (j = 0; j < ballPixels.length; j++) {
                var pointPlayer = playerOnePixels [i];
                var pointBall = ballPixels [j];
                if (pointBall.x === pointPlayer.x && pointBall.y === pointPlayer.y) {
                    return true;
                }
            }
        }
    }
};
playerTwo = {
    x: null,
    y: null,
    width: IMAGES.PLAYER_TWO.WIDTH,
    height: IMAGES.PLAYER_TWO.HEIGHT,
    speed: null,
    name: null,
    points: null,
    coins: null,
    update: function() {
        if (!ki) {
            if (keyData === OPTIONS.KEY_UP && this.y - this.speed > 0 - 10) {
                this.y -= this.speed;
            }
            if (keyData === OPTIONS.KEY_DOWN && this.y + this.speed < HEIGHT_MAIN - this.height + 10) {
                this.y += this.speed;
            }
        }
    },
    draw: function() {
        ctxMain.drawImage(document.getElementById(IMAGES.PLAYER_TWO.PATH), this.x, this.y);
    },
    touch: function() {
        var playerTwoPixels = [];
        var ballPixels = [];
        for (var i = 0; i < this.height; i++) {
            playerTwoPixels.push(new Point(this.x, this.y + i));
        }
        for (i = 0; i < ball.width / 2; i++) {
            for (var j = 0; j < ball.width; j++) {
                ballPixels.push(new Point(ball.x + ball.width / 2 + i, ball.y));
            }
        }
        for (i = 0; i < playerTwoPixels.length; i++) {
            for (var j = 0; j < ballPixels.length; j++) {
                var pointPlayer = playerTwoPixels [i];
                var pointBall = ballPixels [j];
                if (pointBall.x === pointPlayer.x && pointBall.y === pointPlayer.y) {
                    return true;
                }
            }
        }
    }
};
ball = {
    x: null,
    y: null,
    width: IMAGES.BALL.WIDTH,
    speed: null,
    angle: null,
    angleMax: null,
    angleMin: null,
    directionYRandom: null,
    directionX: null,
    directionY: null,
    update: function() {
        for (var i = 0; i < this.speed; i++) {
            if (this.directionX === OPTIONS.BALL_DIRECTION_LEFT) {
                if (playerOne.touch()) {
                    random();
                    this.directionX = OPTIONS.BALL_DIRECTION_RIGHT;
                    if (playSounds) {
                        SOUNDS.PADDEL.play();
                    }
                    this.y -= 1 * this.angle;
                }
                else if (this.x + this.width < 0) {
                    playerTwo.points++;
                    ballReset(playerOne.name);
                    if (playerTwo.points === pointsToWinn) {
                        lostOutput(playerOne.name);
                    }
                }
                else {
                    this.x -= 1;
                }
            }
            else if (this.directionX === OPTIONS.BALL_DIRECTION_RIGHT) {
                if (playerTwo.touch()) {
                    random();
                    this.directionX = OPTIONS.BALL_DIRECTION_LEFT;
                    if (playSounds) {
                        SOUNDS.PADDEL.play();
                    }
                    this.y -= 1 * this.angle;
                }
                else if (this.x > WIDTH_MAIN) {

                    playerOne.points++;
                    ballReset(playerOne.name);
                    if (playerOne.points === pointsToWinn) {
                        lostOutput(playerOne);
                    }
                }
                else {
                    this.x += 1;
                }
            }
            if (this.directionY === OPTIONS.BALL_DIRECTION_UP) {
                if (this.y - this.width / 2 < 0 - this.width / 2) {
                    this.directionY = OPTIONS.BALL_DIRECTION_DOWN;
                    if (playSounds) {
                        SOUNDS.WAND.play();
                    }
                }
                else {
                    this.y -= 1 * this.angle;
                }
                if (this.directionX === OPTIONS.BALL_DIRECTION_RIGHT && playerTwo.y - playerTwo.speed > 0 - 10 && this.y + this.height / 2 !== playerOne.y + this.height / 2) {
                    playerTwo.y -= playerTwo.speed;
                }
            }
            else if (this.directionY === OPTIONS.BALL_DIRECTION_DOWN) {
                if (this.y + this.width > HEIGHT_MAIN) {
                    this.directionY = OPTIONS.BALL_DIRECTION_UP;
                    if (playSounds) {
                        SOUNDS.WAND.play();
                    }
                }
                else {
                    this.y += 1 * this.angle;
                }
                if (this.directionX === OPTIONS.BALL_DIRECTION_RIGHT && playerTwo.y + playerTwo.speed < HEIGHT_MAIN - playerTwo.height + 10 && this.y + this.height / 2 !== playerOne.y + this.height / 2) {
                    playerTwo.y += playerTwo.speed;
                }
            }
        }
    },
    draw: function() {
        ctxMain.drawImage(document.getElementById(IMAGES.BALL.PATH), this.x, this.y);
    }
};

function Box(x, y, width, height, level, size) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.level = level;
    this.size = size;
}

var sec = 0;
var countBoxes = 1;

function boxUpdate() {
    var x, y, height;
    if (time.sec > sec) {
        if (this.sec % 80 === 0 && this.sec !== 0) {
            if (countBoxes + 1 < 6) {
                countBoxes++;
            }
        }
        if (this.sec % 60 === 0 && this.sec !== 0) {
            if (shootCount - 1 >= 0) {
                shootCount--;
                playerOne.speed++;
            }
        }
        if (playerOne.coins % 1000 === 0 && playerOne.coins !== 0) {
            countBoxes++;
        }
        if (time.sec % 7 === 0 && time.sec !== 0) {
            for (var i = 0; i < countBoxes; i++) {
                if (boxes.length < 16) {
                    x = parseInt(Math.random() * (WIDTH_MAIN - 100 - 50) + 50);
                    y = parseInt(Math.random() * (HEIGHT_MAIN - 170 - 50) + 50);
                    height = Math.round((Math.random() * (2 - 1) + 1));

                    switch (height) {
                        case 1:
                            boxes.push(new Box(x, y, BOX_SIZES.BOX_SMALL.WIDTH, BOX_SIZES.BOX_SMALL.HEIGHT, 1, 1));
                            break;
                        case 2:
                            boxes.push(new Box(x, y, BOX_SIZES.BOX_LARGE.WIDTH, BOX_SIZES.BOX_LARGE.HEIGHT, 1, 2));
                            break;
                    }
                }
            }
        }
        else if (time.sec % 30 === 0 && time.sec !== 0) {
            for (var i = 0; i < countBoxes + countBoxes / 2; i++) {
                if (boxes.length < 16) {
                    x = parseInt(Math.random() * (WIDTH_MAIN - 100 - 50) + 50);
                    y = parseInt(Math.random() * (HEIGHT_MAIN - 170 - 50) + 50);
                    height = Math.round((Math.random() * (2 - 1) + 1));
                    switch (height) {
                        case 1:
                            boxes.push(new Box(x, y, BOX_SIZES.BOX_SMALL.WIDTH, BOX_SIZES.BOX_SMALL.HEIGHT, 3, 1));
                            break;
                        case 2:
                            boxes.push(new Box(x, y, BOX_SIZES.BOX_LARGE.WIDTH, BOX_SIZES.BOX_LARGE.HEIGHT, 3, 2));
                            break;
                    }
                }
            }
        }
        else if (time.sec % 15 === 0 && time.sec !== 0) {
            for (var i = 0; i < countBoxes; i++) {
                if (boxes.length < 16) {
                    x = parseInt(Math.random() * (WIDTH_MAIN - 100 - 50) + 50);
                    y = parseInt(Math.random() * (HEIGHT_MAIN - 170 - 50) + 50);
                    height = Math.round((Math.random() * (2 - 1) + 1));

                    switch (height) {
                        case 1:
                            boxes.push(new Box(x, y, BOX_SIZES.BOX_SMALL.WIDTH, BOX_SIZES.BOX_SMALL.HEIGHT, 2, 1));
                            break;
                        case 2:
                            boxes.push(new Box(x, y, BOX_SIZES.BOX_LARGE.WIDTH, BOX_SIZES.BOX_LARGE.HEIGHT, 2, 2));
                            break;
                    }
                }
            }
        }
        sec = time.sec;
    }

    for (var j = 0; j < boxes.length; j++) {
        var currentBox = boxes [j];
        switch (currentBox.level) {
            case 1:
                switch (currentBox.size) {
                    case 1:
                        ctxMain.drawImage(document.getElementById(IMAGES.BOX_SMALL_GREEN.PATH), currentBox.x, currentBox.y);
                        break;
                    case 2:
                        ctxMain.drawImage(document.getElementById(IMAGES.BOX_LARGE_GREEN.PATH), currentBox.x, currentBox.y);
                        break;
                }
                break;
            case 2:
                switch (currentBox.size) {
                    case 1:
                        ctxMain.drawImage(document.getElementById(IMAGES.BOX_SMALL_BLUE.PATH), currentBox.x, currentBox.y);
                        break;
                    case 2:
                        ctxMain.drawImage(document.getElementById(IMAGES.BOX_LARGE_BLUE.PATH), currentBox.x, currentBox.y);
                        break;
                }
                break;
            case 3:
                switch (currentBox.size) {
                    case 1:
                        ctxMain.drawImage(document.getElementById(IMAGES.BOX_SMALL_RED.PATH), currentBox.x, currentBox.y);
                        break;
                    case 2:
                        ctxMain.drawImage(document.getElementById(IMAGES.BOX_LARGE_RED.PATH), currentBox.x, currentBox.y);
                        break;
                }
                break;
        }
    }
}

function bulletUpdate() {
    if (keyData === OPTIONS.KEY_SPACE) {
        bullets.push(new bullet(playerOne.x + playerOne.width, playerOne.y + playerOne.height / 2, 10, OPTIONS.BALL_DIRECTION_RIGHT, true));
        if (playSounds) {
            SOUNDS.LASER_GUN.play();
//            switch (shootCount) {
//                case 3:
//                    SOUNDS.RELOAD_THREE_SEC.play();
//                    break;
//                case 2:
//                    SOUNDS.RELOAD_TWO_SEC.play();
//                    break;
//                case 1:
//                    SOUNDS.RELOAD_ONE_SEC.play();
//                    break;
//            }
        }
    }
    for (var j = 0; j < 10; j++) {
        for (var i = 0; i < bullets.length; i++) {
            var currentBullet = bullets [i];
            if (currentBullet.x !== null) {
                if (currentBullet.direction === OPTIONS.BALL_DIRECTION_RIGHT) {
                    currentBullet.x += 1;
                }
                else if (currentBullet.direction === OPTIONS.BALL_DIRECTION_LEFT) {
                    currentBullet.x -= 1;
                }

                if (currentBullet.x > WIDTH_MAIN || currentBullet.x < 0) {
                    bullets.splice(i, 1);
                }
                bulletTouch();
                ctxMain.drawImage(document.getElementById(IMAGES.BULLET.PATH), currentBullet.x, currentBullet.y);
            }
        }
    }
}

function bulletTouch() {
    bulletsPixels = [];
    bulletPixels = [];
    boxesPixels = [];
    boxPixels = [];
    playerPixels = [];

    for (var i = 0; i < bullets.length; i++) {
        var currentBullet = bullets [i];
        for (var j = 0; j < currentBullet.width; j++) {
            bulletPixels.push(new PointWithIndex(currentBullet.x + currentBullet.width, currentBullet.y + j, i));
        }
        bulletsPixels.push(bulletPixels);
        bulletPixels = new Array();
    }
    for (var k = 0; k < boxes.length; k++) {
        var currentBox = boxes [k];
        for (var l = 0; l < currentBox.height; l++) {
            boxPixels.push(new PointWithIndex(currentBox.x, currentBox.y + l, k));
        }
        boxesPixels.push(boxPixels);
        boxPixels = new Array();
    }

    for (i = 0; i < bulletsPixels.length; i++) {
        var b = bulletsPixels [i];
        for (j = 0; j < b.length; j++) {
            for (k = 0; k < boxesPixels.length; k++) {
                var bo = boxesPixels [k];
                for (l = 0; l < bo.length; l++) {
//                    if (bullets [b [j].index].direction === OPTIONS.BALL_DIRECTION_LEFT) {
//                        for (var m = 0; m < playerOne.height; m++) {
//                            playerPixels.push(new Point(playerOne.x + playerOne.width, playerOne.y + m));
//                        }
//                        for (m = 0; m < playerPixels.length; m++) {
//                            var player = playerPixels [m];
//                            if (b [j].x === player.x && b [j].y === player.y) {
//                                bullets [b [j].index].direction = OPTIONS.BALL_DIRECTION_RIGHT;
//                                bullets [b [j].index].active = true;
//                            }
//                        }
//                    }
                    if (b [j].x === bo [l].x && b [j].y === bo [l].y && bullets [b [j].index].active === true) {
                        if (parseInt(boxes [bo [l].index].level) === 1) {
                            playerOne.coins += 10 * parseInt(boxes [bo [l].index].level);
                            boxes.splice(bo [l].index, 1);
                            if (playSounds) {
                                SOUNDS.EXPLOSION.play();
                                SOUNDS.EXPLOSION.currentTime = 0;
                            }
                            bullets [b [j].index].direction = OPTIONS.BALL_DIRECTION_LEFT;
                            bullets [b [j].index].active = false;
                        }
                        else if (parseInt(boxes [bo [l].index].level) > 1) {
                            if (playSounds) {
                                SOUNDS.EXPLOSION.play();
                                SOUNDS.EXPLOSION.currentTime = 0;
                            }
                            bullets [b [j].index].direction = OPTIONS.BALL_DIRECTION_LEFT;
                            bullets [b [j].index].active = false;
                            playerOne.coins += 10 * parseInt(boxes [bo [l].index].level);
                            boxes [bo [l].index].level--;
                        }
                        return false;
                    }
                }
            }
        }
    }
}

function bullet(x, y, speed, direction, active) {
    this.x = x;
    this.y = y;
    this.width = IMAGES.BULLET.WIDTH;
    this.speed = speed;
    this.direction = direction;
    this.active = active;
}

time = {
    x: null,
    y: null,
    sec: null,
    secBeforeBreak: 0,
    update: function() {
        if (!gameBreak && !lost) {
            this.sec = parseInt((new Date().getTime() - startTime) / 1000) + this.secBeforeBreak;
            if (this.sec % 15 === 0) {
                ball.speed += 0.005;
            }
        }
        else {
            this.secBeforeBreak = this.sec;
        }
    },
    draw: function() {
        ctxInfo.fillStyle = "Black";
        ctxInfo.font = "25px American Typewriter";
        ctxInfo.fillText("Zeit: " + this.sec, this.x, this.y);
    }
};

message = {
    x: null,
    y: null,
    message: null,
    update: function(message) {
        this.message = message;
        ctxInfo.fillStyle = "Black";
        ctxInfo.font = "25px American Typewriter";
        ctxInfo.fillText(this.message, this.x, this.y);
    }
};

coins = {
    x: null,
    y: null,
    draw: function() {
        ctxInfo.fillStyle = "Black";
        ctxInfo.font = "25px American Typewriter";
        ctxInfo.fillText("Coins: " + playerOne.coins + "     " + playerOne.points + " : " + playerTwo.points + "     " + "Coins: " + playerTwo.coins, this.x, this.y);
    }
};

playWithInfo = {
    x: null,
    y: null,
    draw: function() {
        if (playWith === OPTIONS.PLAY_WITH_KEYBOARD) {
            ctxMain.drawImage(document.getElementById(IMAGES.KEYBOARD.PATH), this.x, this.y);
        }
        else if (playWith === OPTIONS.PLAY_WITH_MOUSE_WHEEL) {
            ctxMain.drawImage(document.getElementById(IMAGES.MOUSE.PATH), this.x, this.y);
        }
    }
};

gameOver = {
    x: null,
    y: null,
    draw: function() {
        ctxMain.fillStyle = "White";
        ctxMain.font = "150px American Typewriter";
        ctxMain.fillText("GAME OVER", this.x, this.y + 70);
    }
};

function updateGame() {
    ctxMain.clearRect(0, 0, WIDTH_MAIN, HEIGHT_MAIN);
    ctxMain.drawImage(document.getElementById(IMAGES.BACKGROUND.PATH), 0, 0, WIDTH_MAIN, HEIGHT_MAIN);

    ball.update();
    playerOne.update();
    playerTwo.update();
    boxUpdate();
    bulletUpdate();

    ball.draw();
    playerOne.draw();
    playerTwo.draw();

    keyData = null;
    mouseWheelData = null;
}

function updateInfo(text) {
    time.update();

    ctxInfo.clearRect(0, 0, WIDTH_INFO, HEIGHT_MAIN);

    time.draw();
    coins.draw();
    playWithInfo.draw();
    message.update(text);
}

function random() {
    ball.angle = Math.random() * (ball.angleMax - ball.angleMin) + ball.angleMin;
    ball.directionYRandom = parseInt(Math.random() * (2 - 1) + 1);

    if (ball.directionYRandom === 1) {
        ball.directionY = OPTIONS.BALL_DIRECTION_UP;
    }
    else if (ball.directionYRandom === 2) {
        ball.directionY = OPTIONS.BALL_DIRECTION_DOWN;
    }
}

function lostOutput(player) {
    lost = true;
    SOUNDS.BACKGROUND.pause();
    SOUNDS.GAME_OVER.play();
    if (player === playerOne.name) {
        var data = "nameWon = " + playerTwo.name + " & nameLost = " + playerOne.name + " & coinsWon = " + playerTwo.coins + " & coinsLost = " + playerOne.coins;
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "php/game-over.php", true);
        xhttp.onreadystatechange = function() {
            if (xhttp.readyState === 4 && xhttp.status === 200) {
                console.log(xhttp.responseText);
            }
        };
        xhttp.send(data);
    }
    else if (player === playerTwo.name) {

    }
}

function ballReset(player) {
    ball.x = WIDTH_MAIN / 2;
    ball.y = HEIGHT_MAIN / 2;
    ball.speed = 1.55;
    if (player === playerOne.name) {
        ball.directionX = OPTIONS.BALL_DIRECTION_RIGHT;
    }
    else if (player === playerTwo.name) {
        ball.directionX = OPTIONS.BALL_DIRECTION_LEFT;
    }
    ball.directionY = OPTIONS.BALL_DIRECTION_UP;
    ball.angle = 1.5;
    ball.angleMax = 2;
    ball.angleMin = 0.5;
}

function Point(x, y) {
    this.x = parseInt(x);
    this.y = parseInt(y);
}

function PointWithIndex(x, y, index) {
    this.x = parseInt(x);
    this.y = parseInt(y);
    this.index = index;
}

document.onkeyup = function(evt) {
    var currentTime = parseInt((new Date().getTime() - startTime) / 1000);
    if (currentTime - lastTime >= shootCount) {
        keyData = evt.keyCode;
        lastTime = currentTime;
    }
};

document.onkeydown = function(evt) {
    if (evt.keyCode !== OPTIONS.KEY_SPACE) {
        keyData = evt.keyCode;
    }
    if (keyData === 27) {
        gameBreak = !gameBreak;
    }
    if (keyData === 83) {
        playSounds = !playSounds;
    }
    if (keyData === 13) {
        if (lost) {
            init();
            startTime = new Date().getTime();
            time.secBeforeBreak = 0;
        }
    }
    if (evt.keyCode === 49) {
        playWith = OPTIONS.PLAY_WITH_MOUSE_WHEEL;
        playerOne.speed = 5;
    }
    if (evt.keyCode === 50) {
        playWith = OPTIONS.PLAY_WITH_KEYBOARD;
        playerTwo.speed = 14;
    }
};

function mouseMove() {
    var mY = 0;
    $(document).mousemove(function(e) {
        if (e.screenY < mY) {
            mouseDirectionData = OPTIONS.MOUSE_UP;
        } else {
            mouseDirectionData = OPTIONS.MOUSE_DOWN;
        }
        mousePosY = parseInt(e.screenY - 104);
        mY = e.screenY;
    });
}

function mouseWheelMove() {
    $(window).bind('mousewheel', function(event) {
        if (event.originalEvent.wheelDelta >= 0) {
            mouseWheelData = OPTIONS.MOUSE_WHEEL_UP;
        }
        else {
            mouseWheelData = OPTIONS.MOUSE_WHEEL_DOWN;
        }
    });
}