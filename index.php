<!DOCTYPE html>
<html>
    <head>
        <title>Test</title>
        <!-- Vorbereitung Versiobn 1.5 -->
            <!-- Finns Code: askdhsdkgfsdgk -->
        <!-- Florian Code -->
            <!-- kram -->
        <!-- Anpassungen wegen fehlerhafter tests -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="js/app.js"></script>
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
    </head>
    <body>
        <img src="img/background.png" id="background" style="display: none">
        <img src="img/player_one.png" id="playerOne" style="display: none">
        <img src="img/player_two.png" id="playerTwo" style="display: none">
        <img src="img/ball.png" id="ball" style="display: none">
        <img src="img/bullet.png" id="bullet" style="display: none">
        <img src="img/box_small_green.png" id="box_small_green" style="display: none">
        <img src="img/box_large_green.png" id="box_large_green" style="display: none">
        <img src="img/box_small_blue.png" id="box_small_blue" style="display: none">
        <img src="img/box_large_blue.png" id="box_large_blue" style="display: none">
        <img src="img/box_small_red.png" id="box_small_red" style="display: none">
        <img src="img/box_large_red.png" id="box_large_red" style="display: none">
        <img src="img/mouse.png" id="mouse" style="display: none">
        <img src="img/keyboard.png" id="keyboard" style="display: none">
        <!--Menu-->
        <div data-role="navbar" data-iconpos="bottom">
            <ul>
                <li><a href="#popupLogin" class="ui-btn-active" data-position-to="window" data-rel="popup" data-transition="turn" data-icon="plus">Neues Spiel</a></li>
                <li><a href="#" data-icon="gear">Einstellungen</a></li>
                <li><a href="#" data-icon="info">Über</a></li>
            </ul>
        </div>
        <!--Popup select Game Settings-->
        <div data-role="popup" id="popupLogin" data-theme="a" class="ui-corner-all">
            <div style="padding:100px 100px;">
                     <fieldset data-role="controlgroup" data-type="horizontal">
                            <legend>Spielen mit:</legend>
                            <input type="radio" name="radio-choice-h-2" id="radio-choice-h-2a" value="1" checked="checked">
                            <label for="radio-choice-h-2a">Maus</label>
                            <input type="radio" name="radio-choice-h-2" id="radio-choice-h-2b" value="0">
                            <label for="radio-choice-h-2b">Tastatur</label>
                        </fieldset>
                    Schwierigkeitstufe
                    <input type="range" id="slider-10" min="1" max="5" step="1" value="2">

                    <input type="text" id="one" value="Spieler 1" placeholder="Spieler Eins" data-theme="a">
                    <input type="text" id="two" value="Spieler 2" placeholder="Spieler Zwei" data-theme="a">
                    <button onclick="main(document.getElementById('one').value, document.getElementById('two').value, $('input[name=radio-choice-]:checked').val());" type="submit" class="ui-btn ui-corner-all ui-shadow ui-btn-b ui-btn-icon-left ui-icon-check">Start</button>

            </div>

        </div>
        <canvas width="1100" height="50" id="canvasInfo"></canvas>
        <canvas width="1100" height="600" id="canvasMain"></canvas>
    </body>          
</html>
